"""
Text detection testing using Google ML API
"""
#%% Notes
"""
Google cloud installed using pip install --upgrade google-cloud-vision
"""

#%% Dependencies
import os
import cv2

from text.detect import detect_text, detect_document

#%% Setup
# Set up the Google Cloud API access
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "text/api.json"

# Path to the testing image
image_path = "example_input_2.jpg"
out_path   = "example_output.jpg"

#%% Testing
# detect_text API
if False:
    texts = detect_text(image_path)

    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    i = 0
    for text in texts:
        # Skip the first one
        if i == 0:
            i += 1
            continue

        # Setup up locations
        vertices = [(vertex.x, vertex.y) for vertex in text.bounding_poly.vertices]
        x0y0     = vertices[0]
        x1y1     = vertices[2]
        textx0y0 = (x0y0[0], x0y0[1] - 4)

        # Add a rectangle around the text
        cv2.rectangle(image, x0y0, x1y1, (0, 0, 0), 1)

        # Label the text
        cv2.putText(image, text.description, textx0y0, cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0, 0, 0), 1)

        i += 1

    cv2.imwrite(out_path, cv2.cvtColor(image, cv2.COLOR_RGB2BGR))
    a=0

#%%
image_path = "example_input_6.jpg"
out_path   = "example_output_6.jpg"

# detect_document API
if True:
    texts = detect_document(image_path)

    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    for page in texts.full_text_annotation.pages:
        for block in page.blocks:
            print('\nBlock confidence: {}\n'.format(block.confidence))

            for paragraph in block.paragraphs:
                print('Paragraph confidence: {}'.format(paragraph.confidence))

                for word in paragraph.words:
                    word_text = ''.join([symbol.text for symbol in word.symbols])

                    # Setup up locations
                    vertices = [(vertex.x, vertex.y) for vertex in word.bounding_box.vertices]
                    x0y0 = vertices[0]
                    x1y1 = vertices[2]
                    textx0y0 = (x0y0[0], x0y0[1] - 4)

                    # Add a rectangle around the text
                    cv2.rectangle(image, x0y0, x1y1, (0, 0, 0), 1)

                    # Label the text
                    cv2.putText(image, word_text, textx0y0, cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0, 0, 0), 1)

                    a=0

    cv2.imwrite(out_path, cv2.cvtColor(image, cv2.COLOR_RGB2BGR))

    a = 0

