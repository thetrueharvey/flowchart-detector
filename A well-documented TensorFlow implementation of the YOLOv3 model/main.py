# Main Script

#%% Setup
# Libraries
from os.path import isfile
from glob    import glob

import matplotlib.pyplot as plt
import tensorflow as tf

from tensorflow.keras.models  import load_model
from tensorflow.python.client import device_lib
print([x.name for x in device_lib.list_local_devices() if x.device_type == 'GPU'])

# Custom classes
from config          import *
from dataset.dataset import *
from utils.utils     import *
from models.yolo_v3  import *
#from models.yolo_v3_mobilenet import *
from models.loss     import *

#%% Build Dataset
# Create the dataset if it doesn't exist
if not isfile(tfrecord_path):
    # List of image paths
    image_list = glob(images + "*.jpg")

    # List of objects for each image
    object_list = [parse_annotation( annotation =annotations + image.split("\\")[-1].split('.')[0] + ".xml"
                                   , label_map  =label_map
                                   )
                    for image in image_list
                  ]

    writer = tf.io.TFRecordWriter(tfrecord_path)

    for example in zip(image_list, object_list):
      tf_example = create_tf_example(example)
      writer.write(tf_example.SerializeToString())

    writer.close()

#%% Debug
if False:
    files   = tf.data.Dataset.list_files(tfrecord_path)
    dataset = files.flat_map(tf.data.TFRecordDataset)

    for i,sample in enumerate(dataset):
        temp = parse_tfrecord(sample, feature_map)
        print(i)

#%% Read Dataset
# Dataset creation
train_dataset = load_tfrecord_dataset(tfrecord_path, feature_map)

# Transformations
train_dataset = train_dataset.shuffle(buffer_size=batch_size * 10).batch(batch_size, drop_remainder=True).prefetch(batch_size * 2)

#%% Model
# Get a summary of the model
yolov3.summary()

# Loss
loss = YoloV3Loss( ignore_threshold =ignore_threshold
                 , anchors          =anchors
                 , num_classes      =num_classes
                 , h                =height
                 , w                =width
                 , batch_size       =batch_size
                 )

train_loss = tf.keras.metrics.Mean(name="train_loss")

# Optimizer
optimizer = tf.keras.optimizers.Adam(lr=1e-5)

# Model functions
@tf.function
def train_step(x, y):
    with tf.GradientTape() as tape:
        y_pred = yolov3(x)
        loss_  = loss(y, y_pred)

    gradients = tape.gradient(loss_, yolov3.trainable_variables)
    optimizer.apply_gradients(zip(gradients, yolov3.trainable_variables))

    train_loss(loss_)

    return y_pred

#%% Training
if train:
    if load:
        yolov3 = load_model(model_path)
    for epoch in range(50):
        if epoch % 10 == 0:
            yolov3.save("models/yolo_v3_test_" + str(epoch % 10) + ".h5")

        for images, labels in train_dataset:
            y_ = create_y_true( box_data    =labels.numpy()
                              , anchors     =anchors
                              , num_classes =len(label_map)
                              , h           =height
                              , w           =width
                              )

            y_pred = train_step(images, y_)

            loss_ = loss(y_, y_pred)

            print("Epoch {}, Loss: {}".format(epoch, train_loss.result()))

            train_loss.reset_states()

#%% Detection
yolov3 = load_model(model_path, compile = False)

# Load an image
resized_image, image = process_image(train_image, input_height=416, input_width=416)
x = tf.convert_to_tensor(resized_image, dtype=tf.float32)

# Get a prediction
prediction = yolov3(tf.expand_dims(x, axis=0))

# Reshape
prediction = reshape_model_outputs(prediction, num_classes=len(label_map))

# Postprocessing
boxes          = convert_box_coordinates(prediction)
filtered_boxes = non_max_suppression( boxes
                                    , confidence_threshold=0.5
                                    , iou_threshold       =0.4
                                    )

draw_boxes( output_filename =output_image
          , label_map       =label_map
          , inputs          =filtered_boxes
          , original_image  =image
          , resized_image   =x.numpy()
          )
