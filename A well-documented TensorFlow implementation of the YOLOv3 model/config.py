# Configuration File
#%% config Name
config_name = "Mobile Net"

#%% Dependencies
from tensorflow    import __version__ as tfversion, int64 as tfint64, float32 as tffloat32, string as tfstring
print("TensorFlow version: " + tfversion)

from tensorflow import io

#%%  Label Map
voc_labels = ('aeroplane'
              , 'bicycle'
              , 'bird'
              , 'boat'
              , 'bottle'
              , 'bus'
              , 'car'
              , 'cat'
              , 'chair'
              , 'cow'
              , 'diningtable'
              , 'dog'
              , 'horse'
              , 'motorbike'
              , 'person'
              , 'pottedplant'
              , 'sheep'
              , 'sofa'
              , 'train'
              , 'tvmonitor'
              )

label_map = {k: v + 1 for v, k in enumerate(voc_labels)}
label_map['background'] = 0

#%% Dataset Paths
images      = "F:/Sync/Personal Development/PyTorch ImageNet/pytorch-imagenet/datasets/VOC 2007-2012 processed/images/"
annotations = "F:/Sync/Personal Development/PyTorch ImageNet/pytorch-imagenet/datasets/VOC 2007-2012 processed/annotations/"

tfrecord_path = "dataset/dataset.tfrecord"

#%% Feature Map
feature_map = { 'image/height'            : io.FixedLenFeature([], tfint64)
              , 'image/width'             : io.FixedLenFeature([], tfint64)
              , 'image/filename'          : io.FixedLenFeature([], tfstring)
              , 'image/source_id'         : io.FixedLenFeature([], tfstring)
              , 'image/encoded'           : io.FixedLenFeature([], tfstring)
              , 'image/format'            : io.FixedLenFeature([], tfstring)
              , 'image/object/bbox/xmin'  : io.VarLenFeature(tffloat32)
              , 'image/object/bbox/xmax'  : io.VarLenFeature(tffloat32)
              , 'image/object/bbox/ymin'  : io.VarLenFeature(tffloat32)
              , 'image/object/bbox/ymax'  : io.VarLenFeature(tffloat32)
              , 'image/object/class/text' : io.VarLenFeature(tfstring)
              , 'image/object/class/label': io.VarLenFeature(tfint64)
              }

#%% Image
height = 416
width  = 416

#%% Anchors
anchors      = [(10, 13), (16, 30), (33, 23), (30, 61), (62, 45), (59, 119), (116, 90), (156, 198), (373, 326)]

#%% Loss
ignore_threshold = 0.5

#%% Optimizer

#%% Training
train      = True
load       = False
model_path = "models/yolo_v3_test_0.h5"
batch_size = 8

#%% Inference
train_image = "F:/Sync/Personal Development/PyTorch ImageNet/pytorch-imagenet/datasets/VOC 2007-2012 processed/images/000020.jpg"

output_image = "output.jpg"