# Dependencies
from tensorflow.keras import Model, Input

from models.layers import conv_layer, shortcut_layer

# DarkNet model
model_input = Input(shape=(416,416,3), dtype='float32', name='darknet_input')

x, _        = conv_layer(name='conv_0', x=model_input, filters=32, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = conv_layer(name='conv_1', x=x, filters=64, kernel_size=3, downsample=True, batch_norm=True, activation='LEAKY')

x, _ = conv_layer(name='conv_2', x=x, filters=32, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_3', x=x, filters=64, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = shortcut_layer(name='shortcut_0', shortcut=shortcut, x=x)
x, shortcut = conv_layer(name='conv_4', x=x, filters=128, kernel_size=3, downsample=True, batch_norm=True, activation='LEAKY')

x, _ = conv_layer(name='conv_5', x=x, filters=64, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_6', x=x, filters=128, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_1', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_7', x=x, filters=64, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_8', x=x, filters=128, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = shortcut_layer(name='shortcut_2', shortcut=shortcut, x=x)
x, shortcut = conv_layer(name='conv_9', x=x, filters=256, kernel_size=3, downsample=True, batch_norm=True, activation='LEAKY')

x, _ = conv_layer(name='conv_10', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_11', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_3', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_12', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_13', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_4', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_14', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_15', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_5', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_16', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_17', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_6', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_18', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_19', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_7', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_20', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_21', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_8', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_22', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_23', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_9', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_24', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_25', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, darknet_route_1 = shortcut_layer(name='shortcut_10', shortcut=shortcut, x=x)

x, shortcut = conv_layer(name='conv_26', x=x, filters=512, kernel_size=3, downsample=True, batch_norm=True, activation='LEAKY')

x, _ = conv_layer(name='conv_27', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_28', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_11', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_29', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_30', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_12', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_31', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_32', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_13', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_33', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_34', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_14', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_35', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_36', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_15', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_37', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_38', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_16', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_39', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_40', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_17',shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_41', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_42', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, darknet_route_2 = shortcut_layer(name='shortcut_18',shortcut=shortcut, x=x)

x, shortcut = conv_layer(name='conv_43', x=x, filters=1024, kernel_size=3, downsample=True, batch_norm=True, activation='LEAKY')

x, _ = conv_layer(name='conv_44', x=x, filters=512, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_45', x=x, filters=1024, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_19', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_46', x=x, filters=512, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_47', x=x, filters=1024, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_20', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_48', x=x, filters=512, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_49', x=x, filters=1024, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, shortcut = shortcut_layer(name='shortcut_21', shortcut=shortcut, x=x)

x, _ = conv_layer(name='conv_50', x=x, filters=512, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_51', x=x, filters=1024, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = shortcut_layer(name='shortcut_22', shortcut=shortcut, x=x)

darknet = Model( inputs =model_input
               , outputs=x
               )
