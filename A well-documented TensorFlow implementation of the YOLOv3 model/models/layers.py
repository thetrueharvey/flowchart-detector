# Dependencies
import tensorflow as tf

from tensorflow.keras import layers

# Convolutional layer
def conv_layer(x
               , filters    : int
               , kernel_size: int
               , downsample : bool
               , batch_norm : bool
               , activation : str
               , name       : str
               ):
    """
    Applies convolutional operations for a YOLO V3 model
    """
    if batch_norm:
        use_bias = False
    else:
        use_bias = True

    if downsample:
        paddings = tf.constant([[0, 0], [1, 1], [1, 1], [0, 0]])
        x = tf.pad(x, paddings, 'CONSTANT', name=name + "_pad")
        strides = (2, 2)
        padding = 'valid'
    else:
        strides = (1, 1)
        padding = 'same'

    x = layers.Conv2D(filters=filters
                      , kernel_size=kernel_size
                      , strides=strides
                      , use_bias=use_bias
                      , padding=padding
                      , dtype=tf.float32
                      , name=name
                      )(x)

    if batch_norm:
        x = layers.BatchNormalization(momentum=0.9, epsilon=1e-05, name=name + "_bn")(x)

    if activation == 'LEAKY':
        x = layers.LeakyReLU(alpha=0.1, name=name + "_relu")(x)

    shortcut = x

    return x, shortcut

# Routing layer
def route_layer(x, route, name):
    x = tf.concat(values=[x, route]
                  , axis=-1
                  , name=name
                  )

    return x

# Shortcut layer
def shortcut_layer(x, shortcut, name):
  x = tf.add( x   =x
            , y   =shortcut
            , name=name
            )
  new_shortcut = x

  return x, new_shortcut

# Upsample layer
def upsample_layer(x, name):
    x = tf.image.resize(images  =x
                       , size   =(x.shape[1] * 2, x.shape[2] * 2)
                       , method ="nearest"
                       , name   =name
                       )

    return x

# YOLO layer
def yolo_layer(x
               , anchors
               , num_classes
               , image_height
               , image_width
               , name
               ):
    x_shape = x.get_shape().as_list()

    stride_x = image_width // x_shape[2]
    stride_y = image_height // x_shape[1]

    num_anchors = len(anchors)

    anchors = tf.constant([[a[0] / stride_x, a[1] / stride_y] for a in anchors], dtype=tf.float32)  # convert to scale
    anchors_w = tf.reshape(anchors[:, 0], [1, 1, 1, num_anchors, 1])
    anchors_h = tf.reshape(anchors[:, 1], [1, 1, 1, num_anchors, 1])

    clustroid_x = tf.tile(tf.reshape(tf.range(x_shape[2], dtype=tf.float32), [1, -1, 1, 1]), [x_shape[2], 1, 1, 1])
    clustroid_y = tf.tile(tf.reshape(tf.range(x_shape[1], dtype=tf.float32), [-1, 1, 1, 1]), [1, x_shape[1], 1, 1])

    # [ ? x 13 x 13 x num_anchors x (5 + num_classes)]
    x = tf.reshape(x, [-1, x_shape[1], x_shape[2], num_anchors, num_classes + 5])
    delta_x, delta_y, delta_w, delta_h, obj_conf, class_conf = tf.split(x, [1, 1, 1, 1, 1, num_classes], axis=-1)

    # add grid offsets and multiply by stride to bring up to scale
    box_x = (clustroid_x + tf.nn.sigmoid(delta_x)) * stride_x
    box_y = (clustroid_y + tf.nn.sigmoid(delta_y)) * stride_y
    box_w = anchors_w * tf.exp(delta_w) * stride_x
    box_h = anchors_h * tf.exp(delta_h) * stride_y
    obj_conf = tf.nn.sigmoid(obj_conf)
    class_conf = tf.nn.sigmoid(class_conf)

    x = tf.concat([box_x, box_y, box_w, box_h, obj_conf, class_conf], axis=-1)

    return x
