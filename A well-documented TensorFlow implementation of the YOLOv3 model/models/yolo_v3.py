# Dependencies
from tensorflow.keras import Model

from models.layers import conv_layer, shortcut_layer, route_layer, upsample_layer, yolo_layer
from models.darknet import *

# Setup
num_classes = 21
anchors = [[10,13],  [16,30],  [33,23],  [30,61],  [62,45],  [59,119],  [116,90],  [156,198],  [373,326]]
h = 416
w = 416

# Yolo Model
x, _ = conv_layer(name='conv_52', x=darknet.output, filters=512, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_53', x=x, filters=1024, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_54', x=x, filters=512, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_55', x=x, filters=1024, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, yolo_route = conv_layer(name='conv_56', x=x, filters=512, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_57', x=x, filters=1024, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')

large_object_raw_detections, _ = conv_layer(name='conv_58', x=x, filters=3*(num_classes+5), kernel_size=1, downsample=False, batch_norm=False, activation='LINEAR')
large_object_box_detections = yolo_layer(name='yolo_0', x=large_object_raw_detections, image_height=h, image_width=w, num_classes=num_classes, anchors=anchors[6:9])

x, _ = conv_layer(name='conv_59', x=yolo_route, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x = upsample_layer(name='upsample_0', x=x)
x = route_layer(name='route_0', x=x, route=darknet_route_2)

x, _ = conv_layer(name='conv_60', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_61', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_62', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_63', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, yolo_route = conv_layer(name='conv_64', x=x, filters=256, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_65', x=x, filters=512, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')

medium_object_raw_detections, _ = conv_layer(name='conv_66', x=x, filters=3*(num_classes+5), kernel_size=1, downsample=False, batch_norm=False, activation='LINEAR')
medium_object_box_detections = yolo_layer(name='yolo_1', x=medium_object_raw_detections, image_height=h, image_width=w, num_classes=num_classes, anchors=anchors[3:6])

x, _ = conv_layer(name='conv_67', x=yolo_route, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x = upsample_layer(name='upsample_1', x=x)
x = route_layer(name='route_1', x=x, route=darknet_route_1)

x, _ = conv_layer(name='conv_68', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_69', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_70', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_71', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_72', x=x, filters=128, kernel_size=1, downsample=False, batch_norm=True, activation='LEAKY')
x, _ = conv_layer(name='conv_73', x=x, filters=256, kernel_size=3, downsample=False, batch_norm=True, activation='LEAKY')

small_object_raw_detections, _ = conv_layer(name='conv_74', x=x, filters=3*(num_classes+5), kernel_size=1, downsample=False, batch_norm=False, activation='LINEAR')
small_object_box_detections = yolo_layer(name='yolo_2', x=small_object_raw_detections, image_height=h, image_width=w, num_classes=num_classes, anchors=anchors[0:3])

yolov3 = Model( inputs =darknet.input
              , outputs=[large_object_box_detections, medium_object_box_detections, small_object_box_detections, large_object_raw_detections, medium_object_raw_detections, small_object_raw_detections]
              )
