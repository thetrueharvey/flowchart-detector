import cv2

from xml.etree import ElementTree
from PIL       import Image

import tensorflow        as tf


def parse_annotation(annotation, label_map):
    """
    Converts an annotation from the hardhat dataset to produce the boxes, classes, and difficulties
    :param annotation: str. Path to a .xml annotation file.
    :param label_map: dict. A dictionary that encodes string labels to a numeric value
    :return: list
    """
    # Get the xml root
    root = ElementTree.parse(source=annotation).getroot()

    # Data storage
    boxes        = []
    labels       = []
    classes      = []
    difficulties = []

    # Iterate through the objects in the file
    for obj in root.iter('object'):
      # Difficulty
      difficulty = int(obj.find('difficult').text == '1')

      # Label
      label = obj.find('name').text.lower().strip()

      # Check if the label is in the label map
      if label not in label_map:
          continue

      # Bounding boxes coordinates
      bbox = obj.find('bndbox')
      coords = [int(bbox.find(coord + pos).text) - 1 for pos in ('min', 'max') for coord in ('x', 'y')]

      # Append to storage
      _ = [storage.append(item) for storage, item in
            zip((boxes, labels, classes, difficulties), (coords, label_map[label], label, difficulty))]

    # Return the results
    return {"boxes": boxes, "labels": labels, "classes": classes, "difficulties": difficulties}

# Datatype definitions
def int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def int64_list_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def bytes_list_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))

def float_list_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def create_tf_example(example):
    """
    Example: A list with the format [image path, labels],
           With labels further being a dict with at least ["boxes", "classes", "labels"]
    """
    # Image components
    image = cv2.imread(example[0])

    # TODO(user): Populate the following variables from your example.
    height, width, _   = image.shape # Image dimensions
    filename           = example[0].encode() # Filename of the image. Empty if image is not from file
    image_format       = b'jpeg' # b'jpeg' or b'png'

    # Encoded image data
    encoded_image_data = Image.open(example[0], "r").tobytes() # Encoded image bytes

    # Label Components
    xmins = [(ex[0] / width) * 416 for ex in example[1]["boxes"]] # List of normalized left x coordinates in bounding box (1 per box)
    xmaxs = [(ex[2] / width) * 416 for ex in example[1]["boxes"]] # List of normalized right x coordinates in bounding box
             # (1 per box)
    ymins = [(ex[1] / height) * 416 for ex in example[1]["boxes"]] # List of normalized top y coordinates in bounding box (1 per box)
    ymaxs = [(ex[3] / height) * 416 for ex in example[1]["boxes"]] # List of normalized bottom y coordinates in bounding box
             # (1 per box)
    classes_text = [ex.encode() for ex in example[1]["classes"]] # List of string class name of bounding box (1 per box)
    labels       = [ex for ex in example[1]["labels"]] # List of integer class id of bounding box (1 per box)

    tf_example = tf.train.Example(features=tf.train.Features(feature={
      'image/height'            : int64_feature(height),
      'image/width'             : int64_feature(width),
      'image/filename'          : bytes_feature(filename),
      'image/source_id'         : bytes_feature(filename),
      'image/encoded'           : bytes_feature(encoded_image_data),
      'image/format'            : bytes_feature(image_format),
      'image/object/bbox/xmin'  : float_list_feature(xmins),
      'image/object/bbox/xmax'  : float_list_feature(xmaxs),
      'image/object/bbox/ymin'  : float_list_feature(ymins),
      'image/object/bbox/ymax'  : float_list_feature(ymaxs),
      'image/object/class/text' : bytes_list_feature(classes_text),
      'image/object/class/label': int64_list_feature(labels),
    }))
    return tf_example


def parse_tfrecord(tfrecord, feature_map):
    # Parse a single record
    x = tf.io.parse_single_example(tfrecord, feature_map)

    # X components
    x_train = tf.io.decode_raw(x['image/encoded'], tf.uint8)
    x_train = tf.reshape( x_train
                        , [ x['image/height']
                          , x['image/width']
                          , 3
                          ]
                        )
    x_train = tf.image.resize(x_train / 255, (416, 416))

    # y components
    y_train = tf.stack([ tf.sparse.to_dense(x['image/object/bbox/xmin'])
                       , tf.sparse.to_dense(x['image/object/bbox/ymin'])
                       , tf.sparse.to_dense(x['image/object/bbox/xmax'])
                       , tf.sparse.to_dense(x['image/object/bbox/ymax'])
                       , tf.cast(tf.sparse.to_dense(x['image/object/class/label']), tf.float32)
                       ]
                      , axis=1
                      )

    # Pad the output
    paddings = [[0, 100 - tf.shape(y_train)[0]], [0, 0]]
    y_train = tf.pad(y_train, paddings)

    return x_train, y_train


def load_tfrecord_dataset(file_pattern, feature_map):
    files   = tf.data.Dataset.list_files(file_pattern)
    dataset = files.flat_map(tf.data.TFRecordDataset)

    return dataset.map(lambda x: parse_tfrecord(x, feature_map))
